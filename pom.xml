<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.joliciel.ljtrad</groupId>
  <artifactId>yivo-transcriber</artifactId>
  <version>1.0.1</version>
  <packaging>jar</packaging>
  <name>yivo-transcriber</name>
  <description>rules-based transcription of non-standard Yiddish into YIVO</description>
  <organization>
    <name>Joliciel Informatique SARL</name>
    <url>http://www.joli-ciel.com/</url>
  </organization>
  <scm>
    <developerConnection>urieli</developerConnection>
    <url>https://gitlab.com/ljtrad/yivo-transcriber</url>
    <connection>https://gitlab.com/ljtrad/yivo-transcriber.git</connection>
  </scm>
  <url>https://gitlab.com/ljtrad/yivo-transcriber</url>
  <inceptionYear>2023</inceptionYear>
  <licenses>
    <license>
      <name>GNU AFFERO GENERAL PUBLIC LICENSE, Version 3, 19 November 2007</name>
      <url>http://www.gnu.org/licenses/agpl.txt</url>
      <distribution>repo</distribution>
    </license>
  </licenses>
  <developers>
    <developer>
      <id>urieli</id>
      <name>Assaf Urieli</name>
      <email>assaf.urieli@gmail.com</email>
    </developer>
  </developers>
  <issueManagement>
    <system>github</system>
    <url>https://gitlab.com/ljtrad/yivo-transcriber/-/issues</url>
  </issueManagement>
  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  </properties>
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <version>3.2.0</version>
        <configuration>
          <archive>
            <manifest>
              <addClasspath>true</addClasspath>
              <mainClass>com.joliciel.yivoTranscriber.YivoTranscriber</mainClass>
            </manifest>
          </archive>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.8.1</version>
        <configuration>
          <release>11</release>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>3.0.0-M5</version>
        <configuration>
          <argLine>
            --illegal-access=permit
          </argLine>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-gpg-plugin</artifactId>
        <version>1.5</version>
        <executions>
          <execution>
            <id>sign-artifacts</id>
            <phase>verify</phase>
            <goals>
              <goal>sign</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-deploy-plugin</artifactId>
        <version>2.8.2</version>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-source-plugin</artifactId>
        <version>2.2.1</version>
        <executions>
          <execution>
            <id>attach-sources</id>
            <goals>
              <goal>jar-no-fork</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>3.2.0</version>
        <executions>
          <execution>
            <id>attach-javadocs</id>
            <goals>
              <goal>jar</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <additionalDependencies>
            <additionalDependency>
              <groupId>junit</groupId>
              <artifactId>junit</artifactId>
              <version>4.13.1</version>
            </additionalDependency>
          </additionalDependencies>
          <additionalJOption>-J-Xmx1024m</additionalJOption>
          <detectJavaApiLink>true</detectJavaApiLink>
          <detectLinks>true</detectLinks>
          <doclint>all,-missing</doclint>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>
        <version>3.2.4</version>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>shade</goal>
            </goals>
            <configuration>
              <transformers>
                <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                  <manifestEntries>
                    <Main-Class>com.joliciel.yivoTranscriber.YivoTranscriber</Main-Class>
                  </manifestEntries>
                </transformer>
                <transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
                  <resource>reference.conf</resource>
                </transformer>
                <transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
                  <resource>application.conf</resource>
                </transformer>
              </transformers>
              <shadedArtifactAttached>true</shadedArtifactAttached>
              <shadedClassifierName>shaded</shadedClassifierName>
              <createDependencyReducedPom>false</createDependencyReducedPom>
              <artifactSet>
                <includes>
                  <include>*:*</include>
                </includes>
              </artifactSet>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.12</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>com.typesafe</groupId>
      <artifactId>config</artifactId>
      <version>1.3.0</version>
    </dependency>
    <dependency>
      <groupId>ch.qos.logback</groupId>
      <artifactId>logback-classic</artifactId>
      <version>1.5.3</version>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
      <version>2.0.12</version>
    </dependency>
    <dependency>
      <groupId>net.sf.jopt-simple</groupId>
      <artifactId>jopt-simple</artifactId>
      <version>5.0.4</version>
    </dependency>
    <dependency>
      <groupId>com.github.joniles</groupId>
      <artifactId>rtfparserkit</artifactId>
      <version>1.16.0</version>
    </dependency>
  </dependencies>
  <distributionManagement>
    <snapshotRepository>
      <id>ossrh</id>
      <url>https://oss.sonatype.org/content/repositories/snapshots</url>
    </snapshotRepository>
    <repository>
      <id>ossrh</id>
      <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
    </repository>
  </distributionManagement>
</project>