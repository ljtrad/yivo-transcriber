///////////////////////////////////////////////////////////////////////////////
//Copyright (C) 2019 Assaf Urieli
//
//This file is part of the YIVO Trascriber.
//
//YIVO Trascriber is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//YIVO Trascriber is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with YIVO Trascriber.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
package com.joliciel.yivoTranscriber;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.joliciel.yivoTranscriber.Rule.Action;

public class YivoTranscriberTest {
	private static final Logger LOG = LoggerFactory.getLogger(YivoTranscriberTest.class);

	@Test
	public void testTranscribe() {
		List<Rule> rules = new ArrayList<>();
		rules.add(new Rule("וו", "װ", Action.apply));
		rules.add(new Rule("װאו", "װוּ", Action.apply));
		rules.add(new Rule("על$", "ל", Action.variant));

		Set<String> lexicon = new HashSet<>();
		lexicon.add("װוּנטשל");

		YivoTranscriber yivoTranscriber = new YivoTranscriber(lexicon, rules);
		String word = yivoTranscriber.transcribe("וואונטשעל", false);
		assertEquals("װוּנטשל", word);
	}

	/**
	 * Test that a variant rule constructs all possible permutations in a single
	 * word.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testTranscribeMultipleMatches() throws Exception {
		List<Rule> rules = new ArrayList<>();
		rules.add(new Rule("יי", "ײ", Action.apply));
		rules.add(new Rule("ײ", "ײַ", Action.variant));

		Set<String> lexicon = new HashSet<>();
		lexicon.add("נײַװײַסקײט");

		YivoTranscriber yivoTranscriber = new YivoTranscriber(lexicon, rules);
		String word = yivoTranscriber.transcribe("נייװייסקייט", false);
		assertEquals("נײַװײַסקײט", word);
	}

	/**
	 * Ensure that all words in the sentence are recognised in the lexicon, after
	 * tokenisation.
	 */
	@Test
	public void testTokenize() {
		List<Rule> rules = new ArrayList<>();
		rules.add(new Rule("_", "-", Action.line));

		Set<String> lexicon = new HashSet<>();
		lexicon.add("ס'");
		lexicon.add("האָט");
		lexicon.add("ר'");
		lexicon.add("חײם");

		YivoTranscriber yivoTranscriber = new YivoTranscriber(lexicon, rules);
		String line = yivoTranscriber.transcribeLine("‛ס'האָט ר' חײם‛", true);
		assertEquals("‛ס'האָט ר' חײם‛", line);
	}

	@Test
	public void testRules() throws Exception {
		InputStream rulesStream = YivoTranscriberTest.class.getResourceAsStream("/rules.txt");
		Scanner rulesScanner = new Scanner(rulesStream, "UTF-8");
		List<Rule> rules = Rule.parse(rulesScanner);
		Set<String> lexicon = new HashSet<>();
		lexicon.add("ר'");
		lexicon.add("ייִד");
		lexicon.add("דער");
		lexicon.add("באַמערקט");
		YivoTranscriber yivoTranscriber = new YivoTranscriber(lexicon, rules);
		String line = yivoTranscriber.transcribeLine("דער ייד ר' חיים בעמערקט", true);
		assertEquals("דער ייִד ר' @חײם באַמערקט", line);
	}

	@Test
	public void testTranscribeBook() throws Exception {
		InputStream rulesStream = YivoTranscriberTest.class.getResourceAsStream("/rules.txt");
		Scanner rulesScanner = new Scanner(rulesStream, "UTF-8");
		List<Rule> rules = Rule.parse(rulesScanner);

		File lexiconFile = new File("src/test/resources/Shneur/lexicon.txt");
		File inputFile = new File("src/test/resources/Shneur/Shneur-NonY-5.txt");
		File testFile = new File("src/test/resources/Shneur/Shneur-YIVO-5.txt");
		try (Scanner lexiconScanner = new Scanner(lexiconFile, "UTF-8");
				Scanner inputScanner = new Scanner(inputFile, "UTF-8");
				Scanner testScanner = new Scanner(testFile, "UTF-8");
				Scanner textScanner = new Scanner(inputFile, "UTF-8")
				) {
			Set<String> lexicon = new HashSet<>();
			while (lexiconScanner.hasNextLine()) {
				lexicon.add(lexiconScanner.nextLine());
			}
			YivoTranscriber yivoTranscriber = new YivoTranscriber(lexicon, rules, textScanner);

			while (inputScanner.hasNextLine()) {
				String line = inputScanner.nextLine();
				String testLine = testScanner.nextLine();
				String transcribedLine = yivoTranscriber.transcribeLine(line, false);
				LOG.debug("ָoriginal   : " + line);
				LOG.debug("transcribed: " + transcribedLine);
				assertEquals(testLine, transcribedLine);
			}
		}
	}
}
