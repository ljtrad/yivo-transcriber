# line rules
־	-	line
‛	'	line
-זיין	 זיין	line
דינסט-מויד	דינסטמױד	line
דינסט-מיידל	דינסטמײדל	line
וואוילע-יוּנגען	װױלע יונגען	line
ע'ן	ען	line
-איין	-אײַן	line

# separate combined form into two characters
בּ	בּ	apply
בֿ	בֿ	apply
פּ	פּ	apply
פֿ	פֿ	apply
כּ	כּ	apply
כֿ	כֿ	apply
שׁ	שׁ	apply
שׂ	שׂ	apply
תּ	תּ	apply
וֹ	וֹ	apply
וּ	וּ	apply
אָ	אָ	apply
אַ	אַ	apply
ײַ	ײַ	apply
יִ	יּ	apply

# Tsvey vovn, etc.
וו	װ	apply
וי	ױ	apply
יי(?!ִ)	ײ	apply

# Melupm vov and shtumer aleph
וּאװ	וּװ	apply
ואװ	וּװ	apply
(?<!װ)וּ(?!װ)	ו	apply
װאו	װוּ	apply
װאוּ	װוּ	apply
װאױ	װױ	apply

# Khirik yud
(?<!י)יִ	י	apply

# Shtumer hey
שטעה(?=(ט$|ן$|ען$|$))	שטײ	apply
געה(?=(ט$|ן$|נדיק|נדיקער|נדיקע|נדיקען|נדיקן|$))	גײ	apply
שעהן	שײן	apply
שעהנ(?=(ע|ער|עם)$)	שײנ	apply
זעה(?=(ט$|ען$|ן$|$))	זע	apply
טהוט	טוט	apply
(.)אָהל	$1אָל	apply
אַהל	אַל	apply
אַהן	אַן	apply
אָהן	אָן	apply
אָהר	אָר	apply
איהם	אים	apply
יהן	ין	apply
יהנ	ינ	apply
יהר	יר	apply
ײהן	ײן	apply
ײהנ	ײנ	apply
עהל	על	apply
עהן	ען	apply
עהר	ער	apply
עהנט	ענט	apply
ריהמ	רימ	apply
דרעה	דרײ	apply
^װעה	װײ	apply

# ig = ik
יג(?=($|ע$|ער$|טער$|טע$|טן$))	יק	apply
(?<!(קר|שר))יג(ן|ען)$	יקן	apply

# Khirik yud and shtumer aleph
(?<=ײ|ײַ)אי(?=(ק|קע|קן|קער)$)	יִ	apply

# Hebraic Pey words
^כּפרה$	כּפּרה	apply

# We assume unmarked kof is khof, and dagesh kof needs to be marked
# If rafe khof exists, convert all unmarked khof to kof
כ(?![ּֿ])	כּ	apply	ifExists	כֿ
# Remove rafe from khof
כֿ	כ	apply
# If no khofs are marked, suggest dagesh kof
כ	כּ	variant	ifNotExists	(כּ|כֿ)

# We assume unmarked tof is sof, and dagesh tof needs to be marked
# If rafe sof exists, convert all unmarked sof to tof
ת(?![ּֿ])	תּ	apply	ifExists	תֿ
# Remove rafe from sof
תֿ	ת	apply
# If no tofs are marked, suggest dagesh tof
ת	תּ	variant	ifNotExists	(תּ|תֿ)


# We assume unmarked beys is beys, and rafe veys needs to be marked
# If dagesh beys exists, convert all unmarked beys to rafe veys
ב(?![ּֿ])	בֿ	apply	ifExists	בּ
# Remove dagesh from beys
בּ	ב	apply
# If no beys are marked, suggest rafe veys
ב	בֿ	variant	ifNotExists	(בּ|בֿ)

# We assume both dagesh pey and rafe fey are marked
# If dagesh pey exists, convert all unmarked pey to fey
פ(?![ּֿ])	פֿ	apply	ifExists	פּ
# If rafe fey exists, convert all unmarked fey to pey
פ(?![ּֿ])	פּ	apply	ifExists	פֿ
# If any unmarked pey exists, suggest both dagesh pey and rafe fey
פ(?![ּֿ])	פּ	variant
פ(?![ּֿ])	פֿ	variant


# generic rules
גהעט	געט	apply
ליכ	לעכ	apply
ליך	לעך	apply
^אײנגע	אײַנגע	apply
^אײנצו	אײַנצו	apply
^פֿאַרבײ	פֿאַרבײַ	apply
דים$	דעם	apply
דימלעך$	דעמלעך	apply
(?<!(^\w|נג|נק|מ|נ|צל|דר|ײ|געװ|געז|אױסז|בוטל))ען$	ן	apply
(?<!(^\w|נג|נק|מ|נ|צל|דר|ײ|געװ|געז|אױסז|בוטל|שט|פּ))ענ(?=(ס|דיק|דיקע|דיקן|דיקער|קײט|קײַט|שאַפֿט|שאַפֿטן)$)	נ	apply
(?<!^\w)על$	ל	apply
ײאונג$	ײַונג	apply
^ליעבֿ?	ליב	apply
^אונ(?=ש\w)	אומ	apply
^פֿאַרױ	פֿאַררױ	apply
טעלט$	טלט	apply
יגקײט$	יקײט	apply
(?<!^)געלט$	גלט	apply

# generic words
שטאָדט	שטאָט	apply
שטעדט	שטעט	apply
שכּור	שיכּור	apply
מענש	מענטש	apply
אונז(?=$|ער$|ערע$)	אונדז	apply
רײדן	רעדן	apply
^שען$	שײן	apply
^שענ$	שײנ	apply
^שענעם	שײנעם	apply
^פֿיער	פֿיר	apply
^פֿריה	פֿרי	apply
^כּח$	כּוח	apply
^גבור	גיבור	apply
^גען$	גײן	apply
געשטיק(?=ן|ענע)	געשטיג	apply
דערצעל	דערצײל	apply
^גױעש	גױיִש	apply
^שׂ?מחה	שׂימחה	apply
^טרפֿה	טריפֿה	apply
^זער$	זײער	apply
^װאָנסע	װאָנצע	apply
^רעגענ	רעגנ	apply
^באַלעגאָלעס$	בעלי-עגלות	apply
^באַלעגאָלע$	בעל-עגלה	apply
^באַלעבאָסטע	בעל-הביתטע	apply
^באַלעבאַטי(?=(ם|מלעך))	בעלי-בתּי	apply
^באַלעבאַטי(?=(ש|שע|שער|שן))	בעל-הבתּי	apply
^באַלעבאַס	בעל-הבית	apply
^װאָרים$	װאָרום	apply
^װיפֿיל$	װיפֿל	apply
^דערװײטנס$	דער װײַטנס	apply
פּרובֿ?(?=(ט|ן|טער|טע|טן)$)	פּרוּװ	apply
^לײַ?װענט	לײַװנט	apply
^אפֿילו$	אַפֿילו	apply
^גױם$	גױיִם	apply
^אלמנה$	אַלמנה	apply
^אלמנות$	אַלמנות	apply
פֿינסטער	פֿינצטער	apply
^אָרימע	אָרעמע	apply
^אַ?ודאי	אַװדאי	apply
^אװדאי	אַװדאי	apply

# Id to Yid
^איד$	ייִד	apply
^איד(?=(יש|ישן|ישע|ישער|ישקײט|ישקײַט|ל|עלעך|ן|ענע|ענעס)$)	ייִד	apply

# Add khirik optionally to tsvey yuden
ײ	ייִ	variant
יי	ייִ	variant

# authors inserting ' in between word parts (especially between Hebrew and non-Hebrew parts)
'		variant

# generic variants
^א	אַ	variant
^פאר	פֿאַר	variant
^פֿאר	פֿאַר	variant
^בע	באַ	variant
ײ(?!ַ)	ײַ	variant
פפֿ	פֿ	variant
פּפֿ	פֿ	variant
עלט$	לט	variant
עלער$	לער	variant
עלבױם$	לבױם	variant
נים	נעם	variant
ימער	עמער	variant
(?<=\w)דעם$	דים	variant
או	אָו	variant
ואי	ויִ	variant
ש	שׂ	variant
^פֿיר(?!-)	פֿיר-	variant

# specific rules
^זענט$	זײַט	apply
^זײנט$	זײַט	apply
^נח$	נח	apply
ערש(?=($|ע$|ער$|ען$|ן$))	עריש	apply
^פֿארמאַז	פֿארמאָז	apply
^מעשות$	מעשׂיות	apply
^אַרבײט	אַרבעט	apply
^נאַאיװ	נאַיװ	apply
^פֿרעלע(?=[ךכ])	פֿרײלע	apply
^פּױערש	פּױעריש	apply
^האָמעטנע	האַמעטנע	apply
דערװעלטע	דערװײלטע	apply
^עמיץ	עמעץ	apply
^עמיצ	עמעצ	apply
^אימיץ	עמעץ	apply
^אימיצ	עמעצ	apply
^ערגיץ	ערגעץ	apply

# very specific rules
^הײסגע	הײס-גע	apply
^צוקײלן$	צעקײַלן	apply
^צופּאַטעלט	צעפּאַטלט	apply

# mistakes with dagesh in pey
^פּאַר$	פֿאַר	apply
^פּון$	פֿון	apply

