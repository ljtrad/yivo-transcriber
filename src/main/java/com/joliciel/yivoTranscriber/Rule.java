///////////////////////////////////////////////////////////////////////////////
//Copyright (C) 2019 Assaf Urieli
//
//This file is part of the YIVO Trascriber.
//
//YIVO Trascriber is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//YIVO Trascriber is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with YIVO Trascriber.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
package com.joliciel.yivoTranscriber;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * A single transformation rule, used to transcribe the text.
 * 
 * @author Assaf Urieli
 *
 */
public class Rule {
	public enum Action {
		/**
		 * Always apply this rule.
		 */
		apply,
		/**
		 * Create two variants, one the original, and one with the replacement.
		 */
		variant,
		/**
		 * Apply this rule at line level, rather than word level. Needs to be used if
		 * the regex contains punctuation. Doesn't produce variants.
		 */
		line
	}

	protected final Pattern pattern;
	private final String regex;
	protected final String replacement;
	private final Action action;
	private final boolean testPositive;
	private final Pattern testPattern;
	private boolean activated = false;
	private final Config config = ConfigFactory.load().getConfig("yivo-transcriber");
	private final int maxVariants = config.getInt("max-variants");

	/**
	 * Read rules from a scanner.
	 * <br>
	 * If the line begins with a # or is empty, it is ignored.
	 * Otherwise, the line must at least 3 tabs, as follows:
	 * <ul>
	 *     <li>The regex to match</li>
	 *     <li>The replacement</li>
	 *     <li>apply|variant|line</li>
	 * </ul>
	 *
	 * It can optionally contain two additional tabs, to make the rule conditional:
	 * <ul>
	 *     <li>ifExists|ifNotExists</li>
	 *     <li>The test regex for this condition</li>
	 * </ul>
	 *
	 * @param scanner
	 * @return
	 */
	public static List<Rule> parse(Scanner scanner) {
		List<Rule> rules = new ArrayList<>();
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine().trim();
			if (line.startsWith("#"))
				continue;
			if (line.length() == 0)
				continue;
			String[] parts = line.split("\t");
			Rule rule = null;
			if (parts.length==3) {
				rule = new Rule(parts[0], parts[1], Action.valueOf(parts[2]));
			} else if (parts.length==5) {
				boolean testPositive;
				if (parts[3].equals("ifExists"))
					testPositive = true;
				else if (parts[3].equals("ifNotExists"))
					testPositive = false;
				else
					throw new IllegalArgumentException("Unknown test condition, expected 'ifExists' or 'ifNotExists', found '" + parts[3] + "' on line: " + line);
				rule = new Rule(parts[0], parts[1], Action.valueOf(parts[2]), testPositive, parts[4]);
			} else {
				throw new IllegalArgumentException("Lines must have 3 or 4 tabs, but found " + parts.length + "tabs on line: " + line);
			}
			rules.add(rule);
		}
		return rules;
	}

	public Rule(String regex, String replacement, Action action) {
		this(regex, replacement, action, true, null);
	}

	public Rule(String regex, String replacement, Action action, boolean testPositive, String testRegex) {
		this.regex = regex;
		this.pattern = Pattern.compile(regex, Pattern.UNICODE_CHARACTER_CLASS);
		this.replacement = replacement;
		this.action = action;
		this.testPositive = testPositive;
		this.activated = !testPositive;
		if (testRegex!=null && testRegex.length()>0)
			this.testPattern = Pattern.compile(testRegex, Pattern.UNICODE_CHARACTER_CLASS);
		else {
			this.testPattern = null;
			this.activated = true;
		}
	}

	public Action getAction() {
		return action;
	}

	public String getRegex() {
		return regex;
	}

	public String getReplacement() {
		return replacement;
	}

	/**
	 * Should the test pattern be interpreted as "ifExists"
	 * or "ifNotExists".
	 */
	public boolean isTestPositive() {
		return testPositive;
	}

	/**
	 * The test pattern.
	 * If {@link #isTestPositive()}, the rule is inactive by default, and this activates this rule, when it matches
	 * at least once in the original text.
	 * Otherwise, the rule is active by default, and this de-activates the rule, when it matches at least once
	 * in the original text.
	 */
	public Pattern getTestPattern() {
		return testPattern;
	}

	/**
	 * Is this rule activated for the current text.
	 */
	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	/**
	 * Transform the word.
	 * 
	 * @param word
	 *            the word to transform
	 * @return a list of newly transformed words if the transformation applies, or
	 *         null otherwise
	 */
	public List<String> apply(String word) {
		Matcher matcher = this.pattern.matcher(word);

		if (this.action == Action.variant) {
			// need to create a variant for each time this matches
			List<StringBuffer> variants = new ArrayList<>();
			variants.add(new StringBuffer());
			StringBuffer sb = new StringBuffer();
			int last = 0;
			int offset = 0;
			// |ABABAB|
			// |ABBABBABB|
			while (matcher.find()) {
				if (variants.size() >= maxVariants) {
					break;
				}

				CharSequence fixed = word.subSequence(last, matcher.start());
				CharSequence origAppend = word.subSequence(matcher.start(), matcher.end());
				matcher.appendReplacement(sb, this.replacement);
				CharSequence replAppend = sb.subSequence(matcher.start() + offset, sb.length());
				offset = sb.length() - matcher.end();
				last = matcher.end();

				List<StringBuffer> newVariants = new ArrayList<>(variants.size() * 2);
				for (StringBuffer variant : variants) {
					StringBuffer orig = new StringBuffer(variant);
					orig.append(fixed);
					orig.append(origAppend);
					variant.append(fixed);
					variant.append(replAppend);

					newVariants.add(orig);
					newVariants.add(variant);
				}
				variants = newVariants;
			}
			if (variants.size() == 1)
				return null;

			for (StringBuffer variant : variants) {
				matcher.appendTail(variant);
			}

			List<String> list = new ArrayList<>(variants.size() - 1);
			for (int i = 1; i < variants.size(); i++) {
				StringBuffer variant = variants.get(i);
				list.add(variant.toString());
			}
			return list;

		} else {
			String result = matcher.replaceAll(this.replacement);
			if (word.equals(result)) {
				return null;
			}
			List<String> list = new ArrayList<>(1);
			list.add(result);
			return list;
		}
	}

	@Override
	public String toString() {
		return "Rule{" +
				"regex='" + regex + '\'' +
				", replacement='" + replacement + '\'' +
				", action=" + action +
				'}';
	}
}
