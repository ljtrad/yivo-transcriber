///////////////////////////////////////////////////////////////////////////////
//Copyright (C) 2019 Assaf Urieli
//
//This file is part of the YIVO Trascriber.
//
//YIVO Trascriber is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//YIVO Trascriber is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with YIVO Trascriber.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
package com.joliciel.yivoTranscriber;

import com.joliciel.yivoTranscriber.Rule.Action;
import com.rtfparserkit.converter.text.StringTextConverter;
import com.rtfparserkit.parser.RtfStreamSource;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Transcribes a text from non-YIVO spelling to YIVO spelling.
 * 
 * @author Assaf Urieli
 *
 */
public class YivoTranscriber {
	private static final Logger LOG = LoggerFactory.getLogger(YivoTranscriber.class);
	private final Set<String> lexicon;
	private final List<Rule> rules;
	private final Config config = ConfigFactory.load().getConfig("yivo-transcriber");
	private final Pattern whiteSpaceOrPunct = Pattern.compile("[\\s\\p{Punct}]", Pattern.UNICODE_CHARACTER_CLASS);
	private final Pattern whiteSpaceOrHyphen = Pattern.compile("[\\s-־]", Pattern.UNICODE_CHARACTER_CLASS);
	private final Pattern numberPattern = Pattern.compile("(?U)\\d+");

	private final int maxVariants = config.getInt("max-variants");
	
	/**
	 * Setup the transcriber.
	 * <br>
	 * The rules will be pruned to include only those rules which have no test pattern,
	 * or the rules whose test patterns matches at least once in the text provided.
	 * <br>
	 * When testing the rules, the text will only be scanned up to the number
	 * of characters indicated in the configuration setting "yivo-transcriber.test-size".
	 *
	 * @param lexicon
	 *            the full set of known words
	 * @param rules
	 *            the list of rules to apply
	 * @param text the text to be transcribed, .
	 */
	public YivoTranscriber(Set<String> lexicon, List<Rule> rules, Scanner text) {
		if (lexicon.isEmpty()) {
			lexicon = YivoTranscriber.defaultLexicon();
		}
		if (rules.isEmpty()) {
			rules = YivoTranscriber.defaultRules();
		}
		this.lexicon = lexicon;

		// check which conditional rules are activated by the corpus
		int totalChars = 0;
		int maxChars = config.getInt("test-size");
		if (text!=null) {
			while (text.hasNextLine()) {
				String line = text.nextLine();
				for (Rule rule : rules) {
					// ִ If the rule is only activated when we hit a positive
					// activate it as soon as we match the test pattern.
					// If the rule is activated by default and de-activated when we hit a positive
					// de-activate it as soon as we match the test pattern.
					if (rule.getTestPattern()!=null &&
							((!rule.isActivated()&&rule.isTestPositive()) ||
									(rule.isActivated()&&!rule.isTestPositive()))
					) {
						if (rule.getTestPattern().matcher(line).find()) {
							if (LOG.isDebugEnabled()) {
								LOG.debug("Found " + rule.getTestPattern().pattern() + " in " + line);
							}
							if (rule.isTestPositive()) {
								if (LOG.isDebugEnabled()) {
									LOG.debug("Activating rule " + rule);
								}
								rule.setActivated(true);
							} else {
								if (LOG.isDebugEnabled()) {
									LOG.debug("De-activating rule " + rule);
								}
								rule.setActivated(false);
							}
						}
					}
				}
				totalChars += line.length();
				if (totalChars > maxChars)
					break;
			}
		}

		// add all activated rules
		this.rules = new ArrayList<>();
		for (Rule rule : rules) {
			if (rule.isActivated())
				this.rules.add(rule);
		}
	}

	/**
	 * Create a YivoTranscriber with no conditional rules activated.
	 */
	public YivoTranscriber(Set<String> lexicon, List<Rule> rules) {
		this(lexicon, rules, null);
	}

	/**
	 * Call with default lexicon and rules.
	 */
	public YivoTranscriber() {
		this(Set.of(), List.of());
	}

	/**
	 * Call with default lexicon and rules, and a scanner to analyze the text.
	 */
	public YivoTranscriber(Scanner scanner) {
		this(Set.of(), List.of(), scanner);
	}

	public static Set<String> defaultLexicon() {
		Set<String> all = YivoTranscriber.readLexiconResource(YivoTranscriber.class.getResourceAsStream("/yiddish-lebt-spellchecker.zip"), "UTF-8", true);
		Set<String> common = YivoTranscriber.readLexiconResource(YivoTranscriber.class.getResourceAsStream("/lexicons/common.txt"), "UTF-8", false);
		Set<String> geography = YivoTranscriber.readLexiconResource(YivoTranscriber.class.getResourceAsStream("/lexicons/geography.txt"), "UTF-8", false);
		all.addAll(common);
		all.addAll(geography);
		return all;
	}

	public static List<Rule> defaultRules() {
		return Rule.parse(new Scanner(YivoTranscriber.class.getResourceAsStream("/rules.txt"), StandardCharsets.UTF_8));
	}

	/**
	 * Transcribe a full line into YIVO. First, all rules of type
	 * {@link Action#line} will be applied. Next, the line is tokenized using the
	 * pattern defined in "yivo-transcriber.tokeniser-pattern". Finally, each
	 * individual word is transcribed using {@link #transcribe(String, boolean)}.
	 * 
	 * @param line
	 *            the line to transcribe
	 * @param markUnknown
	 *            whether unknown words should be marked in the transcribed line
	 *            using the pattern taken from configuration setting
	 *            "yivo-transcriber.unknown-word-pattern".
	 * @return the transcribed line
	 */
	public String transcribeLine(String line, boolean markUnknown) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Line: " + line);
		}
		for (Rule rule : rules) {
			if (rule.getAction() == Action.line) {
				List<String> transcribed = rule.apply(line);
				if (transcribed != null) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Applied " + rule.getRegex());
					}
					line = transcribed.get(0);
				}
			}
		}
		Pattern delimiter = Pattern.compile(config.getString("tokeniser-pattern"), Pattern.UNICODE_CHARACTER_CLASS);
		String[] words = delimiter.split(line);
		if (LOG.isDebugEnabled()) {
			LOG.debug("Tokenized: " + String.join("|", words));
		}
		StringBuilder sb = new StringBuilder();
		for (String word : words) {
			sb.append(this.transcribe(word, markUnknown));
		}
		return sb.toString();
	}

	/**
	 * Transcribe a single word into YIVO spelling. <br>
	 * Rules are applied in order of their declaration. <br>
	 * Rules of type {@link Action#apply} are systematically applied. <br>
	 * Rules of type {@link Action#variant} add a possible variant. If the variant
	 * exists in the lexicon, it is automatically accepted and returned. If no
	 * variant exists in the dictionary, the original word with all systematic rules
	 * applied is returned.
	 * 
	 * @param word
	 *            the word to transcribe
	 * @param markUnknown
	 *            if the word is unknown, whether it should be marked using the
	 *            pattern taken from configuration setting
	 *            "yivo-transcriber.unknown-word-pattern".
	 * @return the transcribed word
	 */
	public String transcribe(String word, boolean markUnknown) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Word: " + word);
		}
		// Apply all initial rules
		int i = 0;
		for (i = 0; i < rules.size(); i++) {
			Rule rule = rules.get(i);
			if (rule.getAction() == Action.apply) {
				List<String> newWords = rule.apply(word);
				if (newWords != null) {
					word = newWords.get(0);
					if (LOG.isDebugEnabled()) {
						LOG.debug("Changed word to " + word + ". Rule: " + rule.getRegex());
					}
				}
			} else {
				break;
			}
		}
		// See if transformed word exists
		if (exists(word)) {
			return word;
		}

		// Now apply variants
		List<String> variants = new ArrayList<>();
		variants.add(word);
		for (; i < rules.size(); i++) {
			List<String> newVariants = new ArrayList<>();
			List<String> toCheck = new ArrayList<>();
			Rule rule = rules.get(i);
			switch (rule.getAction()) {
			case apply:
				for (String variant : variants) {
					List<String> newWords = rule.apply(variant);
					if (newWords != null) {
						newVariants.add(newWords.get(0));
						toCheck.add(newWords.get(0));
						if (LOG.isDebugEnabled()) {
							LOG.debug("Changed word to " + newWords.get(0) + ". Rule: " + rule.getRegex());
						}
					} else {
						newVariants.add(variant);
					}
				}
				break;
			case variant:
				if (variants.size() < maxVariants) {
					for (String variant : variants) {
						List<String> newWords = rule.apply(variant);
						if (newWords != null) {
							newVariants.add(variant);
							newVariants.addAll(newWords);
							toCheck.addAll(newWords);

							if (LOG.isDebugEnabled()) {
								List<String> wordsToList = newWords.size() > 100 ? newWords.subList(0, 100) : newWords;
								String maybeThreeDots = newWords.size() > 100 ? ", ..." : "";
								LOG.debug("Added variants: " + String.join(",", wordsToList) + maybeThreeDots + ". Rule: " + rule.getRegex());
							}
						} else {
							newVariants.add(variant);
						}
					}
				} else {
					newVariants = variants;
				}
				break;
			case line:
				// do nothing, rule applied at line level
				continue;
			}
			for (String variant : toCheck) {
				if (exists(variant)) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Found: " + variant);
					}
					return variant;
				}
			}
			variants = newVariants;
		}

		// No new variant exists in lexicon
		// Return the initial word
		String result = variants.get(0);
		if (markUnknown)
			result = this.markUnknown(result);
		return result;
	}

	public boolean exists(String word) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Checking " + word);
		}
		if (word.length() == 0 || whiteSpaceOrPunct.matcher(word).matches())
			return true;
		boolean exists = lexicon.contains(word);
		if (exists)
			return true;
		String[] parts = whiteSpaceOrHyphen.split(word);
		if (parts.length > 1) {
			for (String part : parts) {
				if (!lexicon.contains(part))
					return false;
			}
			return true;
		}
		return false;
	}

	private String markUnknown(String word) {
		if (word.length() > 0 && !whiteSpaceOrPunct.matcher(word).matches() && !numberPattern.matcher(word).matches()) {
			String prefix = config.getString("unknown-word-prefix");
			return prefix + word;
		} else {
			return word;
		}
	}

	public static void main(String[] args) throws Exception {
		OptionParser parser = new OptionParser();
		OptionSpec<Void> serializeOption = parser.accepts("serialize", "serialize the lexicon");
		OptionSpec<Void> helpOption = parser.acceptsAll(Arrays.asList("?", "help"), "show help");
		OptionSpec<File> fileOption = parser.acceptsAll(Arrays.asList("f", "file"), "input file to convert").requiredUnless(helpOption, serializeOption)
				.withRequiredArg().ofType(File.class);
		OptionSpec<String> lexiconOption = parser.acceptsAll(Arrays.asList("l", "lexicon"), "lexicon to recognise known words").requiredIf(serializeOption)
				.withRequiredArg().ofType(String.class);
		OptionSpec<String> lexiconCharsetOption = parser.accepts("lexicon-charset", "the charset for reading the lexicon").withOptionalArg()
				.defaultsTo("UTF-8");
		OptionSpec<String> charsetOption = parser.acceptsAll(Arrays.asList("c", "charset"), "the charset for reading the input file").withOptionalArg()
				.defaultsTo("UTF-8");
		OptionSpec<File> outFileOption = parser.acceptsAll(Arrays.asList("o", "out-file"), "file for output (default: STDOUT)").requiredIf(serializeOption)
				.withRequiredArg().ofType(File.class);
		OptionSpec<File> rulesOption = parser.acceptsAll(Arrays.asList("r", "rules"), "rules file")
				.withRequiredArg().ofType(File.class);

		OptionSpec<Void> markUnknownOption = parser.acceptsAll(Arrays.asList("u", "mark-unknown"), "mark unknown words");
		OptionSet options = parser.parse(args);
		if (args.length == 0 || options.has("help")) {
			parser.printHelpOn(System.out);
			return;
		}

		final Set<String> words;

		if (options.has(lexiconOption)) {
			String lexiconCharset = lexiconCharsetOption.value(options);
			String lexiconPath = lexiconOption.value(options);
			if (lexiconPath.startsWith("http")) {
				words = new HashSet<>();
				try (Scanner scanner = new Scanner(new URL(lexiconPath).openStream(), lexiconCharset)) {
					while (scanner.hasNextLine()) {
						String line = scanner.nextLine();
						if (line.startsWith("#"))
							continue;
						words.add(line);
					}
				}
			} else {
				words = new HashSet<>();
				String[] lexiconPaths = lexiconPath.split(";");
				for (String oneLexiconPath : lexiconPaths) {
					File lexicon = new File(oneLexiconPath);
					if (lexicon.isDirectory()) {
						try (Stream<Path> paths = Files.walk(lexicon.toPath())) {
							paths
									.filter(Files::isRegularFile)
									.map(path -> readLexicon(path.toFile(), lexiconCharset))
									.forEach(set -> words.addAll(set));
						}
					} else {
						words.addAll(readLexicon(lexicon, lexiconCharset));
					}
				}
			}
		} else {
			words = Collections.emptySet();
		}

		File parentDir = outFileOption.value(options).getParentFile();
		if (parentDir != null) {
			parentDir.mkdirs();
		}

		if (options.has(serializeOption)) {
			try (FileOutputStream fos = new FileOutputStream(outFileOption.value(options)); ZipOutputStream zos = new ZipOutputStream(fos);) {
				zos.putNextEntry(new ZipEntry("lexicon.obj"));
				ObjectOutputStream out = new ObjectOutputStream(zos);
				out.writeObject(words);
				out.flush();
			}
		} else {
			List<Rule> rules;
			if (options.has(rulesOption)) {
				Scanner rulesScanner = new Scanner(rulesOption.value(options), "UTF-8");
				rules = Rule.parse(rulesScanner);
			} else {
				rules = Collections.emptyList();
			}

			boolean markUnknown = options.has(markUnknownOption);

			Writer out;
			if (options.has(outFileOption)) {
				out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFileOption.value(options)), "UTF-8"));
			} else {
				out = new BufferedWriter(new OutputStreamWriter(System.out, "UTF-8"));
			}
			
			final File inFile = fileOption.value(options);
			final File textFile;
			if (inFile.getName().endsWith(".rtf")) {
				textFile = File.createTempFile("yivoTranscriber", "txt");
				FileInputStream fis = new FileInputStream(inFile);
				StringTextConverter converter = new StringTextConverter();
				converter.convert(new RtfStreamSource(fis));
				String extractedText = converter.getText();
				
				try (
					Scanner scanner = new Scanner(extractedText);
					Writer writer = new OutputStreamWriter(new FileOutputStream(textFile), StandardCharsets.UTF_8);
					) {
					while (scanner.hasNextLine()) {
						String line = scanner.nextLine();
						if (line.equals("Default Style;"))
							continue;
						writer.write(line + "\n");
					}
				}
			} else {
				textFile = inFile;
			}

			final YivoTranscriber yivoTranscriber;
			try (Scanner scanner = new Scanner(textFile, charsetOption.value(options))) {
				yivoTranscriber = new YivoTranscriber(words, rules, scanner);
			}
			
			try (Scanner scanner = new Scanner(textFile, charsetOption.value(options))) {
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					String transcribed = yivoTranscriber.transcribeLine(line, markUnknown);
					out.write(transcribed + "\n");
					out.flush();
				}
			} finally {
				out.flush();
				out.close();
			}
		}
	}
	
	static Set<String> readLexicon(File lexicon, String lexiconCharset) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Reading from " + lexicon.getPath());
			}
			InputStream inputStream = new FileInputStream(lexicon);
			boolean isZip = lexicon.getName().endsWith(".zip");
			return YivoTranscriber.readLexiconResource(inputStream, lexiconCharset, isZip);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	static Set<String> readLexiconResource(InputStream inputStream, String lexiconCharset, boolean isZip) {
		try {
			final Set<String> words;
			if (isZip) {
				try (ZipInputStream zis = new ZipInputStream(new BufferedInputStream(inputStream))) {
					@SuppressWarnings("unused")
					ZipEntry zipEntry;
					if ((zipEntry = zis.getNextEntry()) != null) {
						ObjectInputStream in = new ObjectInputStream(zis);
						@SuppressWarnings("unchecked")
						Set<String> wordEntry = (Set<String>) in.readObject();
						words = wordEntry;
					} else {
						words = new HashSet<>();
					}
				}
			} else {
				words = new HashSet<>();
				try (Scanner scanner = new Scanner(inputStream, lexiconCharset)) {
					while (scanner.hasNextLine()) {
						String line = scanner.nextLine();
						if (line.startsWith("#"))
							continue;
						String[] allWords = line.split("[\\s\\-]");
						if (allWords.length > 1) {
							for (String word : allWords) {
								words.add(word);
							}
						}
						words.add(line);
					}
				}
			}
			return words;
		} catch (IOException | ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
}
